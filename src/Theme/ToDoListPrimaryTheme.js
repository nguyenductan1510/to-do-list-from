export const ToDoListPrimaryTheme = {
  bgColor: "#fff",
  color: "#343a40",
  borderButton: "0.1rem solid #343a40",
  borderRadiusButton: "none",
  hoverTextColor: "#fff",
  hoverBgColor: "#343a40",
  borderColor: "#343a40",
};
