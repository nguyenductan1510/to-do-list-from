import styled from "styled-components";
//----------------button-------------------
export const Button = styled.button`
  background-color: #27b427;
  color: #fff;
  border: none;
  font-size: 16px;
  padding: 10px 5px;
  border-radius: 8px;
`;
