import React, { Component } from "react";
import { connect } from "react-redux";
import styled, { ThemeProvider } from "styled-components";
import { Button } from "../ComponentToDoList/Button";
import { Heading3 } from "../ComponentToDoList/Heading";
import { Table, Tbody, Td, Th, Thead, Tr } from "../ComponentToDoList/Table";
import { Input, Label } from "../ComponentToDoList/TextField";
import { addSinhVien } from "../redux/action/Action";
class ToDoList extends Component {
  state = {
    id: "",
    name: "",
    telePhone: "",
    email: "",
  };
  renderTbody = () => {
    return this.props.sinhVien.map((item) => {
      return (
        <Tr>
          <Td>{item.id}</Td>
          <Td>{item.name}</Td>
          <Td>{item.telePhone}</Td>
          <Td>{item.email}</Td>

          <Td>
            <Button>
              <i className="fa fa-trash"></i>
            </Button>
          </Td>
        </Tr>
      );
    });
  };
  render() {
    return (
      <div>
        <ThemeProvider theme={this.props}>
          <div className="mb-3">
            <Heading3
              className=" px-3 bg-dark text-white py-4
        "
            >
              Thông Tin Sinh Viên
            </Heading3>
            <div className="row mx-2">
              <div className="col-5 form-group">
                <Label>Mã SV</Label>
                <br />
                <Input
                  onChange={(e) => {
                    this.setState({
                      id: e.target.value,
                    });
                  }}
                  className="w-100"
                ></Input>
              </div>
              <div className="col-5 form-group">
                <Label>Họ tên</Label>
                <br />
                <Input
                  onChange={(e) => {
                    this.setState({
                      name: e.target.value,
                    });
                  }}
                  className="w-100"
                ></Input>
              </div>
              <div className="col-5 form-group">
                <Label>Số điện thoại</Label>
                <br />
                <Input
                  onChange={(e) => {
                    this.setState({
                      telePhone: e.target.value,
                    });
                  }}
                  className="w-100"
                ></Input>
              </div>
              <div className="col-5 form-group">
                <Label>Email</Label>
                <br />
                <Input
                  onChange={(e) => {
                    this.setState({
                      email: e.target.value,
                    });
                  }}
                  className="w-100"
                ></Input>
              </div>
            </div>
            <Button
              onClick={() => {
                let { name, telePhone, email, id } = this.state;

                let newSinhVien = {
                  id: id,
                  name: name,
                  telePhone: telePhone,
                  email: email,
                };
                this.props.dispatch(addSinhVien(newSinhVien));
              }}
              className="mx-4"
            >
              Thêm sinh viên
            </Button>
          </div>
          <div className="px-2 row">
            <Table className="table bg-dark">
              <Thead className="text-white">
                <Th className="col-3">Mã SV</Th>
                <Th className="col-3">Họ tên</Th>
                <Th className="col-3">số điện Thoại</Th>
                <Th className="col-3">email</Th>
              </Thead>
              <Tbody>{this.renderTbody()}</Tbody>
            </Table>
          </div>
        </ThemeProvider>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    sinhVien: state.ToDoReducer.sinhVien,
  };
};

export default connect(mapStateToProps)(ToDoList);
