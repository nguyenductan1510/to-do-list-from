import { addSv } from "../Style/Style";

const initialState = {
  sinhVien: [
    {
      id: "1",
      name: "Nguyễn Văn A",
      telePhone: "005577823",
      email: "nguyenvana@gmail.com",
    },
    {
      id: "2",
      name: "Nguyễn Văn B",
      telePhone: "005577823",
      email: "nguyenvana@gmail.com",
    },
  ],
};

export default (state = initialState, action) => {
  switch (action.type) {
    case addSv: {
      if (action.newSinhVien.name.trim() === "") {
        alert("name is not request");
        return { ...state };
      }
      let updateSinhVien = [...state.sinhVien];
      let index = updateSinhVien.findIndex(
        (sinhVien) => sinhVien.name === action.newSinhVien.name
      );
      if (index !== -1) {
        alert("name already exits");
      }

      updateSinhVien.push(action.newSinhVien);
      state.sinhVien = updateSinhVien;
      return { ...state };
    }
    default:
      return state;
  }
};
